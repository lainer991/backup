#!/bin/bash

export SPARK_MAJOR_VERSION=2
export PYSPARK_PYTHON=/opt/anaconda3/bin/python
export PYSPARK_DRIVER_PYTHON =/opt/anaconda3/bin/python

spark-submit --master yarn \
--deploy-mode client \
--num-executors ${NUM_EXECUTORS} \
--executor-cores 4 \
--conf spark.yarn.appMasterEnv.PYSPARK_DRIVER_PYTHON=/opt/anaconda3/bin/python \
--conf spark.yarn.appMasterEnv.PYSPARK_PYTHON=/opt/anaconda3/bin/python \
--conf spark.sql.hive.verifyPartitionPath=true \
--conf spark.executor.memory=4g \
--conf spark.driver.memory=4g \
--conf spark.yarn.executor.memoryOverhead=2048 \
--conf spark.dynamicAllocation.enabled=true \
--conf spark.shuffle.service.enabled=true \
--conf spark.shuffle.consolidateFiles=true \
--conf spark.akka.frameSize=256 \
--conf spark.broadcast.compress=true \
--conf spark.shuffle.compress=true \
--conf spark.shuffle.spill.compress=true \
--conf spark.rpc.askTimeout=360 \
--queue ${QUEUE_NAME} \
stbms_dpi.py \
${DATE_YEAR} \
${DATE_MONTH} \
${DATE_DAY} \
${REGION} \
${OUTPUT_TABLE}