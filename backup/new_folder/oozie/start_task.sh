#!/usr/bin/env bash

# Set up environment variables for Oozie
#----------------------------------------------------------
OOZIE_URL="http://hd-has015.vimpelcom.ru:11000/oozie"
export OOZIE_URL="${OOZIE_URL}"
NAME_NODE="hdfs://nameservice1"
if [ -z ${PROJECT_NAME} ]; then
    PROJECT_NAME="STBMS_AGG_DAILY"
fi

# Set up Unix FS and HDFS path to config files
#----------------------------------------------------------
HDFS_PROJECT_PATH=/user/${USER}/${PROJECT_NAME}
if [ -z ${PROJECT_DIR} ]; then
    UNIX_PROJECT_PATH="$(dirname "${PWD}")"
else
    UNIX_PROJECT_PATH=${PROJECT_DIR}
fi
WORKING_DIR=${HDFS_PROJECT_PATH}

# Get ticket if not exists
klist || kinit -kt $USER.keytab $USER

# Other settings
#----------------------------------------------------------
if [ -z ${QUEUE_NAME} ]; then
    QUEUE_NAME=adhoc
fi
TABLE_NAME="stbmsout.dpi_data_transform"


if [ -z ${COORD_START_DATE} ]; then
    COORD_START_DATE="2018-10-31T04:00Z"
fi
if [ -z ${COORD_END_DATE} ]; then
    COORD_END_DATE="2118-10-28T04:00Z"
fi

# Do filesystem tasks
#----------------------------------------------------------
echo "Coordinator start date: ${COORD_START_DATE}"
echo "Coordinator end date: ${COORD_END_DATE}"
echo "Working queue: ${QUEUE_NAME}"
echo "Working table: ${TABLE_NAME}"
echo 'Copying Oozie application to HDFS...'
hadoop fs -rm -R ${HDFS_PROJECT_PATH}
hadoop fs -mkdir -p ${HDFS_PROJECT_PATH}
hadoop fs -put ${UNIX_PROJECT_PATH}/oozie/* ${HDFS_PROJECT_PATH}
hadoop fs -ls -R ${HDFS_PROJECT_PATH}
echo 'Done.'


# Run job
#----------------------------------------------------------
echo 'Starting coordinator'
oozie job -verbose \
    -auth KERBEROS \
    -config ${UNIX_PROJECT_PATH}/oozie/job.properties  \
    -D PROJECT_NAME=${PROJECT_NAME} \
    -D NAME_NODE=${NAME_NODE} \
    -D QUEUE_NAME=${QUEUE_NAME} \
    -D WORKING_DIR=${WORKING_DIR} \
    -D TABLE_NAME=${TABLE_NAME} \
    -D COORD_START_DATE=${COORD_START_DATE} \
    -D COORD_END_DATE=${COORD_END_DATE} \
    -run