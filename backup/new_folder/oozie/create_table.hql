set mapreduce.job.queuename=${QUEUE_NAME};

CREATE TABLE IF NOT EXISTS ${TABLE_NAME}(
start_time              string,
calling_no_grp          string,
apn                     string,
subscriber_type         string,
subscriber_market       string,
platform_id             string,
category_id             string,
sub_application_id_grp  string,
edr_count               bigint,
data_volume             bigint,
first_calling_no        string,
last_calling_no         string,
detailed_data           string
)
COMMENT 'Holds data for STBMS project'
PARTITIONED BY (region STRING, create_date STRING, partition_date STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
STORED as SEQUENCEFILE;