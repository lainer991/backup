#!/bin/bash

#------ Define variables --------
arrIN=(${TABLE_FULL_PATH//./ })
DB=${arrIN[0]}
TABLE=${arrIN[1]}

TARGET_DATE=${TARGET_DATE}

#------ Production vars -------
PROD_USER="dmp_stbms"
PROD_TABLE="UCDATPRD.STBMS_DPI_LOAD_TMP"
PROD_ORACLE_HOST="mn-raiddb.vimpelcom.ru"

TEST_USER="BIGDATA_W"
TEST_TABLE="UCDATPRD.STBMS_DPI_LOAD_TMP"
TEST_ORACLE_HOST="mn-raid6-db-pl.vimpelcom.ru"

#------ Run job ----------------
sqoop export \
         -D mapreduce.job.max.split.locations=150 \
         -D mapreduce.job.queuename=${QUEUE_NAME} \
         --connect jdbc:oracle:thin:@${PROD_ORACLE_HOST}:1521:RAID6 \
         --username ${PROD_USER} --password-file hdfs://nameservice1/user/tech_stbmsout_bgd_ms/stbms_write_p \
         --table ${PROD_TABLE} \
         --columns START_TIME,CALLING_NO_GRP,APN,SUBSCRIBER_TYPE,SUBSCRIBER_MARKET,PLATFORM_ID,CATEGORY_ID,SUB_APPLICATION_ID_GRP,EDR_COUNT,DATA_VOLUME,FIRST_CALLING_NO,LAST_CALLING_NO,DETAILED_DATA,REGION,CREATE_DATE \
         --hcatalog-database ${DB} \
         --hcatalog-table ${TABLE} \
         --hive-partition-key 'partition_date' \
         --hive-partition-value ${TARGET_DATE} \
         --input-null-string '\\N' \
         --input-null-non-string '\\N' \

