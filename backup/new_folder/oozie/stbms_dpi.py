# coding: utf-8
import sys
import pyspark
from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.functions import *

from operator import add
from functools import reduce

from pyspark.sql.types import *

from datetime import datetime
import pytz
tz = pytz.timezone('Europe/Moscow')
now_msk = datetime.now(tz).strftime("%Y-%m-%d %H:%M:%S")

spark = SparkSession.builder.enableHiveSupport().getOrCreate()
sc = SparkContext.getOrCreate()
sqlContext = SQLContext(sc)

#set arguments from shell 
year = sys.argv[1]
month = sys.argv[2]
day = sys.argv[3]

region = sys.argv[4]
output_table = sys.argv[5]

#0 dpi data
dpi_table_name = "dpi.dpi_"+region

df_dpi = spark.sql("""
SELECT
from_unixtime(a.start_timestamp_second, 'yyyyMMddHH') START_TIMESTAMP_HOUR,
cast(substr(a.MSISDN, 2, 10) as bigint) MSISDN,
a.APN,
a.PROTOCOL_CATEGORY,
a.SUB_APPLICATION,
a.DOWNLINK_TRAFFIC,
a.UPLINK_TRAFFIC

from {0} a 
WHERE a.year={1}
and a.month={2}
and a.day={3}
""".format(dpi_table_name, year, month, day))

#1 subscriber dict(12kk records)

df_subcr = spark.sql("""
select distinct SUBS_NO
from stbms.stbms_r_subscriber
"""
)


#2 DPI_APPLICATION_GRP dict

df_app = spark.sql("""
SELECT sub_application_id as SUB_APP_ID, sub_application_id_grp as SUB_APP_ID_GRP from stbms.stbms_r_dpi_application_grp
""")


#3 NUMBER_GROUPS dict(2kk records)

df_groups = spark.sql("""
select 
cast(TRIM(FROM_NO)as bigint) FROM_NO,
cast(TRIM(TO_NO)as bigint) TO_NO,
MARKET_CODE
from stbms.stbms_r_number_groups
""")

#4 R_PLATFORM dict

df_platform = spark.sql("""
select MARKET_CODE, PLATFORM_ID, PLATFORM_ID as PLATFORM_ID_MNP
from stbms.stbms_r_platform_id
""")

#5 R_MNP dict
df_mnp = spark.sql("""
with t1 as (
SELECT substr(RN,1,5) RN, min(MARKET_CODE) MARKET_CODE_MNP
FROM stbms.STBMS_R_NUMBER_GROUPS 
WHERE RN IS NOT NULL group by substr(RN,1,5)
)
SELECT t0.*,t1.MARKET_CODE_MNP
from stbms.stbms_r_mnp t0
join t1 ON substr(t0.RN,1,5) = t1.RN""")


# join with broadcast.set broadcast value
from bisect import bisect_right
from pyspark.sql.functions import udf
from pyspark.sql.types import LongType

groups_from = sc.broadcast(df_groups
  .select("FROM_NO")
  .orderBy("FROM_NO") 
  .rdd.flatMap(lambda x: x)
  .collect())

def find_le(x):
    'Find rightmost value less than or equal to x'
    i = bisect_right(groups_from.value, x)
    if i:
        return groups_from.value[i-1]
    return None

df_dpi_with_fromNo = df_dpi.withColumn(
    "FROM_NO", udf(find_le, LongType())("MSISDN"))


df_dpi_market = df_dpi_with_fromNo.join(df_groups, ["FROM_NO"], "left")
df_dpi_market = df_dpi_market.select('MSISDN','START_TIMESTAMP_HOUR','APN','PROTOCOL_CATEGORY','SUB_APPLICATION', 'DOWNLINK_TRAFFIC','UPLINK_TRAFFIC','MARKET_CODE')

# Other joins

# join r_subscriber
df_1 = df_dpi_market.alias('df_1')
df_2 = df_subcr.alias('df_2')
df_subs_type = df_1.join(df_2,df_1.MSISDN == df_2.SUBS_NO, how = 'left').select('df_1.*', 'df_2.SUBS_NO')

# join platform_id
df_1 = df_subs_type.alias('df_1')
df_2 = df_platform.alias('df_2')
df_subs_platform = df_1.join(df_2,df_1.MARKET_CODE == df_2.MARKET_CODE, how = 'left').select('df_1.*', 'df_2.PLATFORM_ID')

# join sub_app_grp
df_1 = df_subs_platform.alias('df_1')
df_2 = df_app.alias('df_2')
df_app_grp = df_1.join(df_2,df_1.SUB_APPLICATION == df_2.SUB_APP_ID, how = 'left').select('df_1.*', 'df_2.SUB_APP_ID_GRP')

# join r_mnp
df_1 = df_app_grp.alias('df_1')
df_2 = df_mnp.alias('df_2')
df_app_mnp = df_1.join(df_2,df_1.MSISDN == df_2.ctn, how = 'left').select('df_1.*', 'df_2.MARKET_CODE_MNP')

# join platform_for_mnp
from pyspark.sql.functions import col
df_1 = df_app_mnp.alias('df_1')
df_2 = df_platform.alias('df_2')
df_full = df_1.join(df_2, col("df_1.MARKET_CODE_MNP") == col("df_2.MARKET_CODE"), how = 'left').select('df_1.*', 'df_2.PLATFORM_ID_MNP')


# select and substr raws
df_to_agg = df_full.select('START_TIMESTAMP_HOUR',\
  'MSISDN',\
  df_full.MSISDN.substr(1, 3).alias("CODE"),\
  'APN',\
  'PROTOCOL_CATEGORY',\
  when(df_full.SUBS_NO.isNotNull(), 'POS').otherwise('PRE').alias('SUBSCRIBER_TYPE'),\
  when(df_full.SUB_APP_ID_GRP.isNotNull(), df_full.SUB_APP_ID_GRP).otherwise('_UN').alias('SUB_APPLICATION_ID_GRP'),\
  when((df_full.MSISDN.isNull())&(df_full.MARKET_CODE.isNull())&(df_full.MARKET_CODE_MNP.isNull()),'_N') \
  .when((df_full.MSISDN.isNotNull())&(df_full.MARKET_CODE.isNull())&(df_full.MARKET_CODE_MNP.isNull()),'_UN') \
  .when((df_full.MSISDN.isNotNull())&(df_full.MARKET_CODE.isNull())&(df_full.MARKET_CODE_MNP.isNotNull()),df_full.MARKET_CODE_MNP) \
  .otherwise(df_full.MARKET_CODE).alias('MARKET_CODE'),\
  
  when((df_full.MARKET_CODE.isNull())&(df_full.MARKET_CODE_MNP.isNull())&(df_full.PLATFORM_ID.isNull()),'_N') \
  .when(((df_full.MARKET_CODE.isNotNull())|(df_full.MARKET_CODE_MNP.isNotNull()))&(df_full.PLATFORM_ID_MNP.isNull()),'_UN') \
  .when(((df_full.MARKET_CODE.isNotNull())|(df_full.MARKET_CODE_MNP.isNotNull()))&(df_full.PLATFORM_ID_MNP.isNotNull()),df_full.PLATFORM_ID_MNP) \
  .otherwise(df_full.PLATFORM_ID).alias('PLATFORM_ID'),\
  'DOWNLINK_TRAFFIC','UPLINK_TRAFFIC')

from pyspark.sql import functions as F

# group by final df

df_agg = df_to_agg.groupBy('START_TIMESTAMP_HOUR','CODE','APN','PROTOCOL_CATEGORY','SUBSCRIBER_TYPE','SUB_APPLICATION_ID_GRP','MARKET_CODE','PLATFORM_ID').agg(sum('DOWNLINK_TRAFFIC'), sum('UPLINK_TRAFFIC'), count('MSISDN'),min('MSISDN'),max('MSISDN'))
df_agg = df_agg.withColumn('DATA_VOLUME',reduce(add, [F.col(x) for x in ["sum(UPLINK_TRAFFIC)","sum(DOWNLINK_TRAFFIC)"]]))
df_agg = df_agg.withColumn("FIRST_CALLING_NO", df_agg["min(MSISDN)"].cast(StringType()))
df_agg = df_agg.withColumn("LAST_CALLING_NO", df_agg["max(MSISDN)"].cast(StringType()))
df_agg = df_agg.withColumn("DETAILED_DATA",lit("_N"))
# final select

df = df_agg.select(col('START_TIMESTAMP_HOUR').alias('START_TIME'),
col('CODE').alias('CALLING_NO_GRP'),
'APN',
'SUBSCRIBER_TYPE',
col('MARKET_CODE').alias('SUBSCRIBER_MARKET'),
'PLATFORM_ID',
col('PROTOCOL_CATEGORY').alias('CATEGORY_ID'),
'SUB_APPLICATION_ID_GRP',
col('count(MSISDN)').alias('EDR_COUNT'),
'DATA_VOLUME',
'FIRST_CALLING_NO',
'LAST_CALLING_NO',
'DETAILED_DATA')

# load to table by partition

df.createOrReplaceTempView("stbms_temp_view")
sqlContext.sql("""
insert overwrite table {0} partition(region = '{1}', create_date = '{2}', partition_date = '{3}-{4}-{5}') 
select * from stbms_temp_view""".format(output_table, str(region).upper(), now_msk, year,month,day))


